# Debian Buster based image for usage with AWS Fargate Custom Executor

A Docker image ready to run the simplest jobs with the [AWS Fargate Custom Executor
driver](https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/fargate). 

It has the AWS CLI installed

